import { Entity, model, property, hasOne } from '@loopback/repository';
import { Licensee } from './licensee.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'member' } }
})
export class Member extends Entity {
  @property({
    type: 'string',
    required: false,
    id: 1,
    postgresql: { columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'name', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'surname', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  surname: string;

  @hasOne(() => Licensee, { keyTo: "memberid" })
  licensee: Licensee;

  @property({
    type: 'boolean',
    required: false,
    postgresql: { columnName: 'deleted', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'FALSE' },
  })
  deleted: boolean;

  // Define well-known properties here
  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Member>) {
    super(data);
  }
}

export interface MemberRelations {
  // describe navigational properties here
}

export type MemberWithRelations = Member & MemberRelations;
