import { Entity, model, property, belongsTo, hasOne } from '@loopback/repository';
import { Licensee } from './licensee.model';
import { Weapon } from './weapon.model';
import { User } from './user.model';

@model({ settings: { idInjection: false, postgresql: { schema: 'public', table: 'loan' } } })
export class Loan extends Entity {
  @property({
    type: 'date',
    defaultFn: 'now',
    postgresql: { columnName: 'exitdate', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  exitdate?: string;

  @property({
    type: 'string',
    required: false,
    id: 1,
    postgresql: { columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  id: string;

  @property({
    type: 'date',
    postgresql: { columnName: 'returndate', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  returndate?: string;

  @belongsTo(() => Licensee, { name: 'licenseeloans' })
  licenseeid: string;

  @belongsTo(() => Weapon, { name: 'weaponloans' })
  weaponid: string;

  @property({
    type: 'string',
    postgresql: { columnName: 'reason', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
  })
  reason?: string;

  @belongsTo(() => User, { name: 'initiatorRelation' })
  initiator: string;

  @belongsTo(() => User, { name: 'returnInitiatorRelation' })
  returnInitiator: string;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Loan>) {
    super(data);
  }
}

export interface LoanRelations {
  // describe navigational properties here
}

export type LoanWithRelations = Loan & LoanRelations;
