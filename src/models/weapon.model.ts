import { Entity, model, property, hasMany } from '@loopback/repository';
import { Loan } from './loan.model';

export enum HandType {
  RIGHT_HANDED = "rightHanded",
  LEFT_HANDED = "leftHanded",
  AMBIDEXTROUS = "ambidextrous",
  NOT_DEFINED = "undefined"
}

export enum WeaponType {
  RIFLE = "rifle",
  PISTOL = "pistol",
  CANNON = "cannon",
  CARBINE = "carbine",
  CROSSBOW = "crossbow",
  REVOLVER = "revolver",
  NOT_DEFINED = "undefined"
}

export enum CaliberType {
  C4_5 = "4.5mm",
  C7_5 = "7.5mm",
  C22LR = ".22LR",
  C38 = ".38",
  ARROW = "arrow",
  LEVER = "lever",
  NOT_DEFINED = "undefined"
}

export enum CategoryType {
  A = "A",
  B = "B",
  C = "C",
  D = "D",
  NOT_DEFINED = "undefined"
}

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'weapon' } }
})
export class Weapon extends Entity {
  @property({
    type: 'string',
    required: false,
    id: 1,
    postgresql: { columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'serialnumber', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
    jsonSchema: {
      minLength: 1
    }
  })
  serialnumber: string;

  @hasMany(() => Loan, { keyTo: 'weaponid' })
  loansweapon: Loan[];

  @property({
    type: 'string',
    postgresql: { columnName: 'model', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      nullable: true
    }
  })
  model?: string;

  @property({
    type: 'boolean',
    required: false,
    postgresql: { columnName: 'available', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  available: boolean;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'description', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      nullable: true
    }
  })
  description?: string;

  @property({
    type: 'boolean',
    required: false,
    postgresql: { columnName: 'deleted', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  deleted: boolean;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'handtype', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      type: ["string", "null"],
      nullable: true,
      enum: Object.values(HandType),
    }
  })
  handtype?: HandType;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'weapontype', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      type: ["string", "null"],
      enum: Object.values(WeaponType),
      nullable: true
    }
  })
  weaponType?: WeaponType;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'calibertype', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      type: ["string", "null"],
      enum: Object.values(CaliberType),
      nullable: true
    }
  })
  caliber?: CaliberType;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'category', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      type: ["string", "null"],
      enum: Object.values(CategoryType),
      nullable: true
    }
  })
  category?: CategoryType;

  @property({
    type: 'number',
    required: false,
    postgresql: { columnName: 'price', dataType: 'real', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      nullable: true
    }
  })
  price?: number;

  @property({
    type: 'string',
    required: false,
    postgresql: { columnName: 'clubnumber', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES' },
    jsonSchema: {
      nullable: true
    }
  })
  clubNumber: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Weapon>) {
    super(data);
  }
}



export interface WeaponRelations {
  // describe navigational properties here
}

export type WeaponWithRelations = Weapon & WeaponRelations;
