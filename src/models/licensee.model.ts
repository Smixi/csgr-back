import { Entity, model, property, hasOne, hasMany, belongsTo } from '@loopback/repository';
import { Member } from './member.model';
import { Loan } from './loan.model';

@model({
  settings: { idInjection: false, postgresql: { schema: 'public', table: 'licensee' } }
})
export class Licensee extends Entity {
  @property({
    type: 'string',
    required: false,
    id: 1,
    postgresql: { columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'licensenumber', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  licensenumber: string;

  @belongsTo(() => Member, { name: 'member' })
  memberid: string;

  @property({
    type: 'boolean',
    required: false,
    postgresql: { columnName: 'deleted', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'FALSE' },
  })
  deleted: boolean;

  @hasMany(() => Loan, { keyTo: 'licenseeid' })
  loanslicensee: Loan[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Licensee>) {
    super(data);
  }
}

export interface LicenseeRelations {
  // describe navigational properties here
}

export type LicenseeWithRelations = Licensee & LicenseeRelations;
