// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { Entity, model, property, hasMany, hasOne } from '@loopback/repository';
import { UserCredentials } from './user-credentials.model';
import { Role, basicAuthorization } from '../services/basic.authorizor';

@model({
  settings: {
    idInjection: false,
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    postgresql: { columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'email', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' },
  })
  email: string;

  @property({
    type: 'boolean',
    required: false,
    postgresql: { columnName: 'isdeleted', dataType: 'boolean', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' }
  })
  isDeleted: boolean;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'role', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' }
  })
  role: Role;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'name', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' }
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    postgresql: { columnName: 'surname', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO' }
  })
  surname: string;

  @hasOne(() => UserCredentials)
  userCredentials: UserCredentials;

  constructor(data?: Partial<User>) {
    super(data);
  }
}
