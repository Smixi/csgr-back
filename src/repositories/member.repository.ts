import {DefaultCrudRepository, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {Member, MemberRelations, Licensee} from '../models';
import {PostgresDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {LicenseeRepository} from './licensee.repository';

export class MemberRepository extends DefaultCrudRepository<
  Member,
  typeof Member.prototype.id,
  MemberRelations
> {

  public readonly licensee: HasOneRepositoryFactory<Licensee, typeof Member.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('LicenseeRepository') protected licenseeRepositoryGetter: Getter<LicenseeRepository>,
  ) {
    super(Member, dataSource);
    this.licensee = this.createHasOneRepositoryFactoryFor('licensee', licenseeRepositoryGetter);
    this.registerInclusionResolver('licensee', this.licensee.inclusionResolver);
  }
}
