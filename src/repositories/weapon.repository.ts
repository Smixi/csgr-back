import { DefaultCrudRepository, repository, HasManyRepositoryFactory, DefaultTransactionalRepository } from '@loopback/repository';
import { Weapon, WeaponRelations, Loan } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { LoanRepository } from './loan.repository';

export class WeaponRepository extends DefaultTransactionalRepository<
  Weapon,
  typeof Weapon.prototype.id,
  WeaponRelations
  > {

  public readonly loansweapon: HasManyRepositoryFactory<Loan, typeof Weapon.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('LoanRepository') protected loanRepositoryGetter: Getter<LoanRepository>,
  ) {
    super(Weapon, dataSource);
    this.loansweapon = this.createHasManyRepositoryFactoryFor('loansweapon', loanRepositoryGetter);
    this.registerInclusionResolver('loansweapon', this.loansweapon.inclusionResolver);
  }

}
