import { DefaultCrudRepository, BelongsToRepository, HasOneRepositoryFactory, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import { Licensee, LicenseeRelations, Member, Loan} from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import {MemberRepository} from './member.repository';
import {LoanRepository} from './loan.repository';

export class LicenseeRepository extends DefaultCrudRepository<
  Licensee,
  typeof Licensee.prototype.id,
  LicenseeRelations
  > {

  public readonly member: BelongsToAccessor<Member, typeof Licensee.prototype.id>;

  public readonly loanslicensee: HasManyRepositoryFactory<Loan, typeof Licensee.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('MemberRepository') protected memberRepositoryGetter: Getter<MemberRepository>, @repository.getter('LoanRepository') protected loanRepositoryGetter: Getter<LoanRepository>,
  ) {
    super(Licensee, dataSource);
    this.loanslicensee = this.createHasManyRepositoryFactoryFor('loanslicensee', loanRepositoryGetter,);
    this.registerInclusionResolver('loanslicensee', this.loanslicensee.inclusionResolver);
    this.member = this.createBelongsToAccessorFor('member', memberRepositoryGetter,);
    this.registerInclusionResolver('member', this.member.inclusionResolver);
  }
}
