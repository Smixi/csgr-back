import { DefaultCrudRepository, repository, BelongsToAccessor, DefaultTransactionalRepository, HasOneRepositoryFactory } from '@loopback/repository';
import { Loan, LoanRelations, Licensee, Weapon, User } from '../models';
import { PostgresDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { LicenseeRepository } from './licensee.repository';
import { WeaponRepository } from './weapon.repository';
import { UserRepository } from './user.repository';

export class LoanRepository extends DefaultTransactionalRepository<
  Loan,
  typeof Loan.prototype.id,
  LoanRelations
  > {

  public readonly licenseeloans: BelongsToAccessor<Licensee, typeof Loan.prototype.id>;

  public readonly weaponloans: BelongsToAccessor<Weapon, typeof Loan.prototype.id>;

  public readonly initiator: BelongsToAccessor<User, typeof Loan.prototype.id>;
  public readonly returnInitiator: BelongsToAccessor<User, typeof Loan.prototype.id>;

  constructor(
    @inject('datasources.postgres') dataSource: PostgresDataSource, @repository.getter('LicenseeRepository') protected licenseeRepositoryGetter: Getter<LicenseeRepository>, @repository.getter('WeaponRepository') protected weaponRepositoryGetter: Getter<WeaponRepository>, @repository.getter('UserRepository') protected userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(Loan, dataSource);
    this.initiator = this.createBelongsToAccessorFor('initiatorRelation', userRepositoryGetter);
    this.registerInclusionResolver('initiator', this.initiator.inclusionResolver);
    this.returnInitiator = this.createBelongsToAccessorFor('returnInitiatorRelation', userRepositoryGetter);
    this.registerInclusionResolver('returnInitiator', this.returnInitiator.inclusionResolver);
    this.weaponloans = this.createBelongsToAccessorFor('weaponloans', weaponRepositoryGetter);
    this.registerInclusionResolver('weaponloans', this.weaponloans.inclusionResolver);
    this.licenseeloans = this.createBelongsToAccessorFor('licenseeloans', licenseeRepositoryGetter);
    this.registerInclusionResolver('licenseeloans', this.licenseeloans.inclusionResolver);
  }

  async findInitiator(
    userId: typeof User.prototype.id,
  ): Promise<User> {
    return this.initiator(userId);
  }

  async findReturnInitiator(
    userId: typeof User.prototype.id,
  ): Promise<User> {
    return this.returnInitiator(userId)
  }

}
