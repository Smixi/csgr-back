import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Licensee,
  Loan,
} from '../models';
import { LicenseeRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class LicenseeLoanController {
  constructor(
    @repository(LicenseeRepository) protected licenseeRepository: LicenseeRepository,
  ) { }

  @authenticate("jwt")
  @get('/licensees/{id}/loans', {
    responses: {
      '200': {
        description: 'Array of Licensee has many Loan',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Loan) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Loan>,
  ): Promise<Loan[]> {
    return this.licenseeRepository.loanslicensee(id).find(filter);
  }

  @authenticate("jwt")
  @post('/licensees/{id}/loans', {
    responses: {
      '200': {
        description: 'Licensee model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Loan) } },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Licensee.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Loan, {
            title: 'NewLoanInLicensee',
            exclude: ['id'],
            optional: ['licenseeid']
          }),
        },
      },
    }) loan: Omit<Loan, 'id'>,
  ): Promise<Loan> {
    return this.licenseeRepository.loanslicensee(id).create(loan);
  }

  @authenticate("jwt")
  @patch('/licensees/{id}/loans', {
    responses: {
      '200': {
        description: 'Licensee.Loan PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Loan, { partial: true }),
        },
      },
    })
    loan: Partial<Loan>,
    @param.query.object('where', getWhereSchemaFor(Loan)) where?: Where<Loan>,
  ): Promise<Count> {
    return this.licenseeRepository.loanslicensee(id).patch(loan, where);
  }

  @authenticate("jwt")
  @del('/licensees/{id}/loans', {
    responses: {
      '200': {
        description: 'Licensee.Loan DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Loan)) where?: Where<Loan>,
  ): Promise<Count> {
    return this.licenseeRepository.loanslicensee(id).delete(where);
  }
}
