import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Weapon,
  Loan,
} from '../models';
import { WeaponRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class WeaponLoanController {
  constructor(
    @repository(WeaponRepository) protected weaponRepository: WeaponRepository,
  ) { }

  @authenticate("jwt")
  @get('/weapons/{id}/loans', {
    responses: {
      '200': {
        description: 'Array of Weapon has many Loan',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Loan) },
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Loan>,
  ): Promise<Loan[]> {
    return this.weaponRepository.loansweapon(id).find(filter);
  }

  @authenticate("jwt")
  @post('/weapons/{id}/loans', {
    responses: {
      '200': {
        description: 'Weapon model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Loan) } },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Weapon.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Loan, {
            title: 'NewLoanInWeapon',
            exclude: ['id'],
            optional: ['weaponid']
          }),
        },
      },
    }) loan: Omit<Loan, 'id'>,
  ): Promise<Loan> {
    return this.weaponRepository.loansweapon(id).create(loan);
  }

  @authenticate("jwt")
  @patch('/weapons/{id}/loans', {
    responses: {
      '200': {
        description: 'Weapon.Loan PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Loan, { partial: true }),
        },
      },
    })
    loan: Partial<Loan>,
    @param.query.object('where', getWhereSchemaFor(Loan)) where?: Where<Loan>,
  ): Promise<Count> {
    return this.weaponRepository.loansweapon(id).patch(loan, where);
  }

  @authenticate("jwt")
  @del('/weapons/{id}/loans', {
    responses: {
      '200': {
        description: 'Weapon.Loan DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Loan)) where?: Where<Loan>,
  ): Promise<Count> {
    return this.weaponRepository.loansweapon(id).delete(where);
  }
}
