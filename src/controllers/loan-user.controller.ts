import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Loan,
  User,
} from '../models';
import { LoanRepository } from '../repositories';

export class LoanUserController {
  constructor(
    @repository(LoanRepository)
    public loanRepository: LoanRepository,
  ) { }

  @get('/loans/{id}/initiator', {
    responses: {
      '200': {
        description: 'User belonging to Loan',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(User) },
          },
        },
      },
    },
  })
  async getUser(
    @param.path.string('id') id: typeof Loan.prototype.id,
  ): Promise<User> {
    return this.loanRepository.findInitiator(id);
  }
}
