import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  FilterBuilder,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
} from '@loopback/rest';
import { Licensee } from '../models';
import { LicenseeRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class LicenseeController {
  constructor(
    @repository(LicenseeRepository)
    public licenseeRepository: LicenseeRepository,
  ) { }

  @authenticate("jwt")
  @post('/licensees', {
    responses: {
      '200': {
        description: 'Licensee model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Licensee) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Licensee, {
            title: 'NewLicensee',
            exclude: ['id'],
          }),
        },
      },
    })
    licensee: Omit<Licensee, 'id'>,
  ): Promise<Licensee> {
    let filterBuilder = new FilterBuilder<Licensee>();
    return this.licenseeRepository.findOne(filterBuilder.where({ "licensenumber": licensee.licensenumber }).filter).then((lic) => {
      return lic !== null ? Promise.reject(new HttpErrors[400]("Ce numéro de licence est déjà utilisé")) : this.licenseeRepository.create(licensee)
    })
  }

  @authenticate("jwt")
  @get('/licensees/count', {
    responses: {
      '200': {
        description: 'Licensee model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Licensee)) where?: Where<Licensee>,
  ): Promise<Count> {
    return this.licenseeRepository.count(where);
  }

  @authenticate("jwt")
  @get('/licensees', {
    responses: {
      '200': {
        description: 'Array of Licensee model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Licensee, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Licensee)) filter?: Filter<Licensee>,
  ): Promise<Licensee[]> {
    return this.licenseeRepository.find(filter);
  }

  @authenticate("jwt")
  @patch('/licensees', {
    responses: {
      '200': {
        description: 'Licensee PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Licensee, { partial: true }),
        },
      },
    })
    licensee: Licensee,
    @param.query.object('where', getWhereSchemaFor(Licensee)) where?: Where<Licensee>,
  ): Promise<Count> {
    return this.licenseeRepository.updateAll(licensee, where);
  }

  @authenticate("jwt")
  @get('/licensees/{id}', {
    responses: {
      '200': {
        description: 'Licensee model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Licensee, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Licensee)) filter?: Filter<Licensee>
  ): Promise<Licensee> {
    return this.licenseeRepository.findById(id, filter);
  }

  @authenticate("jwt")
  @patch('/licensees/{id}', {
    responses: {
      '204': {
        description: 'Licensee PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Licensee, { partial: true }),
        },
      },
    })
    licensee: Licensee,
  ): Promise<void> {
    await this.licenseeRepository.updateById(id, licensee);
  }

  @authenticate("jwt")
  @put('/licensees/{id}', {
    responses: {
      '204': {
        description: 'Licensee PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() licensee: Licensee,
  ): Promise<void> {
    await this.licenseeRepository.replaceById(id, licensee);
  }

  @authenticate("jwt")
  @del('/licensees/{id}', {
    responses: {
      '204': {
        description: 'Licensee DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.licenseeRepository.findById(id).then((licensee) => {
      licensee.deleted = true;
      this.licenseeRepository.updateById(id, licensee)
    })
  }

  @authenticate("jwt")
  @get('/licensees/ByLicenseNumber/{licenseNumber}', {
    responses: {
      '200': {
        description: 'Licensee model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Licensee, { includeRelations: true }),
          },
        },
      },
    }
  })
  async findByLicenseNumber(
    @param.path.string('licenseNumber') id: string
  ): Promise<Licensee> {
    let filterBuilder: FilterBuilder = new FilterBuilder()
    return this.licenseeRepository.findOne(filterBuilder.where({ "licensenumber": id }).filter).then((licensee) => {
      if (licensee == null) {
        throw new HttpErrors[404]("No licensee with this license number")
      }
      else {
        return licensee;
      }
    });
  }
}
