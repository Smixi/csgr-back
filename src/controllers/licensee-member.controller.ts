import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Licensee,
  Member,
} from '../models';
import { LicenseeRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class LicenseeMemberController {
  constructor(
    @repository(LicenseeRepository)
    public licenseeRepository: LicenseeRepository,
  ) { }

  @authenticate("jwt")
  @get('/licensees/{id}/member', {
    responses: {
      '200': {
        description: 'Member belonging to Licensee',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Member) },
          },
        },
      },
    },
  })
  async getMember(
    @param.path.string('id') id: typeof Licensee.prototype.id,
  ): Promise<Member> {
    return this.licenseeRepository.member(id);
  }
}
