import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  Member,
  Licensee,
} from '../models';
import { MemberRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class MemberLicenseeController {
  constructor(
    @repository(MemberRepository) protected memberRepository: MemberRepository,
  ) { }

  @authenticate("jwt")
  @get('/members/{id}/licensee', {
    responses: {
      '200': {
        description: 'Member has one Licensee',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Licensee),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<Licensee>,
  ): Promise<Licensee> {
    return this.memberRepository.licensee(id).get(filter);
  }

  @authenticate("jwt")
  @post('/members/{id}/licensee', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Licensee) } },
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof Member.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Licensee, {
            title: 'NewLicenseeInMember',
            exclude: ['id'],
            optional: ['memberId']
          }),
        },
      },
    }) licensee: Omit<Licensee, 'id'>,
  ): Promise<Licensee> {
    return this.memberRepository.licensee(id).create(licensee);
  }

  @authenticate("jwt")
  @patch('/members/{id}/licensee', {
    responses: {
      '200': {
        description: 'Member.Licensee PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Licensee, { partial: true }),
        },
      },
    })
    licensee: Partial<Licensee>,
    @param.query.object('where', getWhereSchemaFor(Licensee)) where?: Where<Licensee>,
  ): Promise<Count> {
    return this.memberRepository.licensee(id).patch(licensee, where);
  }

  @authenticate("jwt")
  @del('/members/{id}/licensee', {
    responses: {
      '200': {
        description: 'Member.Licensee DELETE success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(Licensee)) where?: Where<Licensee>,
  ): Promise<Count> {
    return this.memberRepository.licensee(id).delete(where);
  }
}
