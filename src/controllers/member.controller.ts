import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import { Member } from '../models';
import { MemberRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class MemberController {
  constructor(
    @repository(MemberRepository)
    public memberRepository: MemberRepository,
  ) { }
  @authenticate("jwt")
  @post('/members', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Member) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Member, {
            title: 'NewMember',
            exclude: ['id'],
          }),
        },
      },
    })
    member: Omit<Member, 'id'>,
  ): Promise<Member> {
    return this.memberRepository.create(member);
  }
  @authenticate("jwt")
  @get('/members/count', {
    responses: {
      '200': {
        description: 'Member model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Member)) where?: Where<Member>,
  ): Promise<Count> {
    return this.memberRepository.count(where);
  }
  @authenticate("jwt")
  @get('/members', {
    responses: {
      '200': {
        description: 'Array of Member model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Member, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Member)) filter?: Filter<Member>,
  ): Promise<Member[]> {
    return this.memberRepository.find(filter);
  }
  @authenticate("jwt")
  @patch('/members', {
    responses: {
      '200': {
        description: 'Member PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Member, { partial: true }),
        },
      },
    })
    member: Member,
    @param.query.object('where', getWhereSchemaFor(Member)) where?: Where<Member>,
  ): Promise<Count> {
    return this.memberRepository.updateAll(member, where);
  }
  @authenticate("jwt")
  @get('/members/{id}', {
    responses: {
      '200': {
        description: 'Member model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Member, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Member)) filter?: Filter<Member>
  ): Promise<Member> {
    return this.memberRepository.findById(id, filter);
  }
  @authenticate("jwt")
  @patch('/members/{id}', {
    responses: {
      '204': {
        description: 'Member PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Member, { partial: true }),
        },
      },
    })
    member: Member,
  ): Promise<void> {
    await this.memberRepository.updateById(id, member);
  }
  @authenticate("jwt")
  @put('/members/{id}', {
    responses: {
      '204': {
        description: 'Member PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() member: Member,
  ): Promise<void> {
    await this.memberRepository.replaceById(id, member);
  }

  @authenticate("jwt")
  @del('/members/{id}', {
    responses: {
      '204': {
        description: 'Member DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.memberRepository.findById(id).then((member) => {
      member.deleted = true;
      this.memberRepository.updateById(id, member);
    })
  }
}
