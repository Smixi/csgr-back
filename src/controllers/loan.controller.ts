import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  FilterBuilder,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  HttpErrors,
  toExpressPath,
} from '@loopback/rest';
import { Loan, LoanWithRelations } from '../models';
import { LoanRepository, LicenseeRepository, WeaponRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { UserProfile, securityId, SecurityBindings } from '@loopback/security'
import { inject } from '@loopback/core';
import * as _ from "lodash"

const includedRelations = {
  include: [
    {

      relation: "licenseeloans",
      scope: {
        include: [
          {
            relation: "member"
          }
        ]
      }
    },
    {
      relation: "weaponloans"
    },
    {
      relation: "initiator"
    },
    {
      relation: "returnInitiator"
    }
  ]
}


export class LoanController {
  constructor(
    @repository(LoanRepository)
    public loanRepository: LoanRepository,
    @repository(LicenseeRepository)
    public licenseeRepository: LicenseeRepository,
    @repository(WeaponRepository)
    public weaponRepository: WeaponRepository,
  ) { }

  @authenticate("jwt")
  @post('/loans', {
    responses: {
      '200': {
        description: 'Borrow a weapon using its id and the one of the licensee',
        content: { 'application/json': { schema: getModelSchemaRef(Loan) } },
      },
      '400': {
        description: 'Return a weapon borrowed by a user',
        content: { 'application/json': { schema: getModelSchemaRef(Loan) } },
      },
    },
  })
  async borrowWeapon(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Loan, {
            title: 'NewLoan',
            exclude: ['id', 'exitdate', 'returndate'],
          }),
        },
      },
    })
    loan: Omit<Loan, 'id'>,
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Loan> {
    return this.createLoan(loan, currentUserProfile);
  }

  @authenticate("jwt")
  @post('/loans/{id}/Return', {
    responses: {
      '204': {
        description: 'Return a borrowed weapon using its id',
        content: { 'application/json': { schema: getModelSchemaRef(Loan) } },
      },
      '400': {
        description: 'Borrowed weapon by a user is not returned',
      },
    },
  })
  async returnWeapon(
    @param.path.string('id') id: string,
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile,
  ): Promise<Loan> {
    return this.loanRepository.findById(id)
      .then((loan) => {
        return this.returnLoan(loan, currentUserProfile).then((returnLoan) => {
          return returnLoan;
        });
      })
    /*if (loan.returndate === null) {
      loan.returndate = new Date().toISOString();
      return this.loanRepository.updateById(id, (loan as Loan));
    }
    else {
      throw new HttpErrors[400]("You cannot borrow a weapon that is not borrowed")
    }
  })*/
  }

  @authenticate("jwt")
  @get('/loans/count', {
    responses: {
      '200': {
        description: 'Loan model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Loan)) where?: Where<Loan>,
  ): Promise<Count> {
    return this.loanRepository.count(where);
  }

  @authenticate("jwt")
  @get('/loans', {
    responses: {
      '200': {
        description: 'Array of Loan model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Loan, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Loan)) filter?: Filter<Loan>,
  ): Promise<Loan[]> {
    return this.loanRepository.find(filter);
  }

  @authenticate("jwt")
  @get('/loans/{id}', {
    responses: {
      '200': {
        description: 'Loan model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Loan, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Loan)) filter?: Filter<Loan>
  ): Promise<Loan> {
    return this.loanRepository.findById(id, filter);
  }

  /*async isWeaponAvailable(weaponId: string): Promise<boolean> {
    let filterBuilder = new FilterBuilder<Loan>()
    let filterAvailable = filterBuilder.order("exitdate DESC").where({ "weaponid": weaponId }).filter
    return this.weaponRepository.exists(weaponId).then((exist) => {
      if (exist) {
        return this.loanRepository.findOne(filterAvailable)
          .then((lastLoanForWeapon) => {
            if (lastLoanForWeapon === null || lastLoanForWeapon?.returndate !== null) {
              return Promise.resolve(true);
            }
            else {
              return Promise.resolve(false)
            }
          })
      }
      else {
        throw new HttpErrors[400]("Weapon doesn't exists")
      }

    })

  }*/

  public async createLoan(loan: Omit<Loan, "id">, currentUser: UserProfile): Promise<LoanWithRelations> {

    const tx = await this.loanRepository.beginTransaction();
    //Find weapon
    try {
      const weapon = await this.weaponRepository.findById(loan.weaponid)
      //Update weapon availability if available.
      if (weapon.available == false) {
        throw new HttpErrors[400]("Impossible d'emprunter une arme qui n'est pas disponible");
      }

      weapon.available = false;
      await this.weaponRepository.update(weapon, { transaction: tx });
      const licensee = await this.licenseeRepository.findById((loan.licenseeid), undefined, { transaction: tx });
      (loan as LoanWithRelations).initiator = currentUser[securityId];
      const newLoan = await this.loanRepository.create(loan, { transaction: tx });
      await tx.commit();
      const newLoanWithRelation = await this.loanRepository.findById(newLoan.id, _.cloneDeep(includedRelations));
      return newLoanWithRelation;
    } catch (err) {
      console.log(err)
      throw new HttpErrors[400](err)
    }
  }

  public returnLoan(loan: Loan, currentUser: UserProfile): Promise<LoanWithRelations> {

    return this.loanRepository.beginTransaction()
      .then(tx => {
        //Find weapon
        return this.weaponRepository.findById(loan.weaponid)
          .then(weapon => {
            //Update weapon availability if not available.
            if (weapon.available == true) {
              throw new HttpErrors[400]("Impossible de rendre une arme qui n'est pas emprunté");
            }
            weapon.available = true;
            return this.weaponRepository.update(weapon, { transaction: tx });
          })
          .then(() => {
            loan.returndate = new Date().toISOString();
            loan.returnInitiator = currentUser[securityId];
            return this.loanRepository.update(loan, { transaction: tx })
          })
          .then(() => {
            return tx.commit().then(() => {
              return this.loanRepository.findById(loan.id,
                Object.assign({}, _.cloneDeep(includedRelations)))
            })
          }).catch((err) => {
            console.log(err)
            return tx.rollback().then(() => { throw new HttpErrors[400](err) });
          })
      })
  }



}
