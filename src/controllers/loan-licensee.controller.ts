import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Loan,
  Licensee,
} from '../models';
import { LoanRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class LoanLicenseeController {
  constructor(
    @repository(LoanRepository)
    public loanRepository: LoanRepository,
  ) { }

  @authenticate("jwt")
  @get('/loans/{id}/licensee', {
    responses: {
      '200': {
        description: 'Licensee belonging to Loan',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Licensee) },
          },
        },
      },
    },
  })
  async getLicensee(
    @param.path.string('id') id: typeof Loan.prototype.id,
  ): Promise<Licensee> {
    return this.loanRepository.licenseeloans(id);
  }
}
