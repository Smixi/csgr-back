import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Loan,
  Weapon,
} from '../models';
import { LoanRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';

export class LoanWeaponController {
  constructor(
    @repository(LoanRepository)
    public loanRepository: LoanRepository,
  ) { }

  @authenticate("jwt")
  @get('/loans/{id}/weapon', {
    responses: {
      '200': {
        description: 'Weapon belonging to Loan',
        content: {
          'application/json': {
            schema: { type: 'array', items: getModelSchemaRef(Weapon) },
          },
        },
      },
    },
  })
  async getWeapon(
    @param.path.string('id') id: typeof Loan.prototype.id,
  ): Promise<Weapon> {
    return this.loanRepository.weaponloans(id);
  }
}
