import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
  FilterBuilder,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getFilterSchemaFor,
  getModelSchemaRef,
  getWhereSchemaFor,
  patch,
  put,
  del,
  requestBody,
  getJsonSchemaRef,
  HttpErrors,
} from '@loopback/rest';
import { Weapon, Loan } from '../models';
import { WeaponRepository, LoanRepository } from '../repositories';
import { inject } from '@loopback/core';
import { LoanController } from './loan.controller';
import { isNullOrUndefined } from 'util';
import { authenticate } from '@loopback/authentication';

export class WeaponController {
  constructor(
    @repository(WeaponRepository)
    public weaponRepository: WeaponRepository,
    @repository(LoanRepository)
    public loanRepository: LoanRepository,
    @inject('controllers.LoanController')
    public loanController: LoanController
  ) { }

  @authenticate('jwt')
  @post('/weapons', {
    responses: {
      '200': {
        description: 'Weapon model instance',
        content: { 'application/json': { schema: getModelSchemaRef(Weapon) } },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Weapon, {
            title: 'NewWeapon',
            exclude: ['id'],
          }),
        },
      },
    })
    weapon: Omit<Weapon, 'id'>,
  ): Promise<Weapon> {
    let checkIfExist = await this.weaponRepository.findOne({ where: { serialnumber: weapon.serialnumber } });
    if (!!checkIfExist) {
      throw new HttpErrors[400]("Il n'est pas possible d'avoir deux armes avec le même numéro de série")
    }
    return this.weaponRepository.create(weapon);
  }

  @authenticate("jwt")
  @get('/weapons/count', {
    responses: {
      '200': {
        description: 'Weapon model count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async count(
    @param.query.object('where', getWhereSchemaFor(Weapon)) where?: Where<Weapon>,
  ): Promise<Count> {
    return this.weaponRepository.count(where);
  }

  @authenticate("jwt")
  @get('/weapons', {
    responses: {
      '200': {
        description: 'Array of Weapon model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Weapon, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.query.object('filter', getFilterSchemaFor(Weapon)) filter?: Filter<Weapon>,
  ): Promise<Weapon[]> {
    return this.weaponRepository.find(filter);
  }

  @authenticate("jwt")
  @patch('/weapons', {
    responses: {
      '200': {
        description: 'Weapon PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Weapon, { partial: true }),
        },
      },
    })
    weapon: Weapon,
    @param.query.object('where', getWhereSchemaFor(Weapon)) where?: Where<Weapon>,
  ): Promise<Count> {
    return this.weaponRepository.updateAll(weapon, where);
  }

  @authenticate("jwt")
  @get('/weapons/{id}', {
    responses: {
      '200': {
        description: 'Weapon model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Weapon, { includeRelations: true }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.query.object('filter', getFilterSchemaFor(Weapon)) filter?: Filter<Weapon>
  ): Promise<Weapon> {
    return this.weaponRepository.findById(id, filter);
  }

  @authenticate("jwt")
  @patch('/weapons/{id}', {
    responses: {
      '204': {
        description: 'Weapon PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Weapon, { partial: true }),
        },
      },
    })
    weapon: Weapon,
  ): Promise<void> {
    await this.weaponRepository.updateById(id, weapon);
  }

  @authenticate("jwt")
  @put('/weapons/{id}', {
    responses: {
      '204': {
        description: 'Weapon PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() weapon: Weapon,
  ): Promise<void> {
    await this.weaponRepository.replaceById(id, weapon);
  }

  @authenticate("jwt")
  @del('/weapons/{id}', {
    responses: {
      '204': {
        description: 'Weapon DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.weaponRepository.findById(id).then((weapon) => {
      weapon.deleted = true;
      this.weaponRepository.updateById(id, weapon);
    })
  }

  @authenticate("jwt")
  @get('/weapons/{id}/GetAvailability', {
    responses: {
      '200': {
        description: 'Weapon model instance',
        content: {
          'text/plain:':
          {
            schema: {
              type: 'string',
              example: 'pong'
            }
          },
        }
      }
    }
  })
  async findByIdAvailability(
    @param.path.string('id') id: string
  ): Promise<boolean> {
    return this.weaponRepository.findById(id).then(wpn => {
      return wpn.available;
    });
  }

  @authenticate("jwt")
  @get('/weapons/BySerialNumber/{serialNumber}', {
    responses: {
      '200': {
        description: 'Weapon model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Weapon, { includeRelations: true }),
          },
        },
      },
    }
  })
  async findBySerialNumber(
    @param.path.string('serialNumber') id: string,
    @param.query.object('filter', getFilterSchemaFor(Weapon)) filter?: Filter<Weapon>
  ): Promise<Weapon> {
    let filterBuilder: FilterBuilder = new FilterBuilder()
    let wpnFilter = filterBuilder.where({ "serialnumber": id }).filter;
    if (filter != null || filter != undefined) {
      filter.where = filterBuilder.where({ "serialnumber": id }).filter.where;
      wpnFilter = filter
    }
    return this.weaponRepository.findOne(wpnFilter).then((wpn) => {
      if (wpn == null) {
        throw new HttpErrors[404]("Aucune arme avec ce numéro de série");
      }
      else {
        return wpn;
      }
    }
    );
  }
}
