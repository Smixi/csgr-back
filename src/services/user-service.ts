// Copyright IBM Corp. 2019,2020. All Rights Reserved.
// Node module: loopback4-example-shopping
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

import { HttpErrors } from '@loopback/rest';
import { Credentials, UserRepository } from '../repositories/user.repository';
import { User } from '../models/user.model';
import { UserService } from '@loopback/authentication';
import { UserProfile, securityId } from '@loopback/security';
import { repository } from '@loopback/repository';
import { PasswordHasher } from './hash.password.bcryptjs';
import { PasswordHasherBindings } from '../keys';
import { inject } from '@loopback/context';
import { validateCredentials } from "./validator"

export class MyUserService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const invalidCredentialsError = 'Mauvais mot de passe ou adresse email.';
    const deletedAccount = "Votre compte est suspendu"
    const foundUser = await this.userRepository.findOne({
      where: { email: credentials.email },
    });
    if (!foundUser) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const credentialsFound = await this.userRepository.findCredentials(
      foundUser.id,
    );
    if (!credentialsFound) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    const passwordMatched = await this.passwordHasher.comparePassword(
      credentials.password,
      credentialsFound.password,
    );

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    if (foundUser.isDeleted) {
      throw new HttpErrors.Unauthorized(deletedAccount);
    }

    return foundUser;
  }

  convertToUserProfile(user: User): UserProfile {
    // since first name and lastName are optional, no error is thrown if not provided
    let userName = user.email;
    const userProfile = {
      [securityId]: user.id,
      name: userName,
      id: user.id,
      role: user.role
    };

    return userProfile;
  }

  async changePassword(userId: string, password: string) {
    const foundUser = await this.userRepository.findById(userId);
    if (foundUser == null) {
      throw new HttpErrors.NotFound
    }

    const cred: Credentials = { email: foundUser.email, password: password };
    try {
      validateCredentials(cred);
      const hash = await this.passwordHasher.hashPassword(password);

      this.userRepository.updateCredentials(foundUser.id, { email: foundUser.email, password: hash });
    } catch (err) {
      throw err
    }


  }
}
