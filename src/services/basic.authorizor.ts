import {
  AuthorizationContext,
  AuthorizationMetadata,
  AuthorizationDecision,
} from '@loopback/authorization';
import _ from 'lodash';
import { UserProfile, securityId } from '@loopback/security';

export enum Role {
  USER = "user",
  ADMIN = "admin",
  SUPERADMIN = "superadmin"
}

// Instance level authorizer
// Can be also registered as an authorizer, depends on users' need.
export async function basicAuthorization(
  authorizationCtx: AuthorizationContext,
  metadata: AuthorizationMetadata,
): Promise<AuthorizationDecision> {
  // No access if authorization details are missing
  let currentUser: UserProfile;
  if (authorizationCtx.principals.length > 0) {
    const user = _.pick(authorizationCtx.principals[0], [
      'id',
      'name',
      'role'
    ]);
    currentUser = { [securityId]: user.id, name: user.name, role: user.role };

    if (metadata.allowedRoles?.includes(currentUser.role)) {
      return AuthorizationDecision.ALLOW;
    }
    else {
      return AuthorizationDecision.DENY;
    }

  } else {
    return AuthorizationDecision.DENY;
  }

  return AuthorizationDecision.ALLOW;
}
