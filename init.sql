--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3 (Debian 12.3-1.pgdg100+1)
-- Dumped by pg_dump version 12.2

-- Started on 2020-06-13 18:42:24 UTC

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3033 (class 1262 OID 17675)
-- Name: dev; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE dev WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';


\connect dev

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 17684)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 3034 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 4 (class 3079 OID 17693)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 3035 (class 0 OID 0)
-- Dependencies: 4
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- TOC entry 3 (class 3079 OID 17730)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3036 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 207 (class 1259 OID 17741)
-- Name: licensee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.licensee (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    licensenumber text NOT NULL,
    memberid uuid,
    deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 208 (class 1259 OID 17749)
-- Name: loan; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.loan (
    exitdate timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    returndate timestamp with time zone,
    weaponid uuid NOT NULL,
    licenseeid uuid NOT NULL,
    reason text,
    initiator uuid NOT NULL,
    returninitiator uuid
);


--
-- TOC entry 209 (class 1259 OID 17756)
-- Name: member; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.member (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    surname text NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 206 (class 1259 OID 17678)
-- Name: migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    run_on timestamp without time zone NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 17676)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3037 (class 0 OID 0)
-- Dependencies: 205
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 210 (class 1259 OID 17764)
-- Name: user; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email text NOT NULL
);


--
-- TOC entry 211 (class 1259 OID 17771)
-- Name: usercredentials; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.usercredentials (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    userid uuid NOT NULL,
    password text NOT NULL
);


--
-- TOC entry 212 (class 1259 OID 17778)
-- Name: weapon; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.weapon (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    serialnumber text NOT NULL,
    model text,
    available boolean DEFAULT true NOT NULL,
    description text,
    deleted boolean DEFAULT false NOT NULL,
    handtype text,
    weapontype text,
    calibertype text,
    category text,
    price real,
    clubnumber text
);


--
-- TOC entry 2856 (class 2604 OID 17681)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 3022 (class 0 OID 17741)
-- Dependencies: 207
-- Data for Name: licensee; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.licensee (id, licensenumber, memberid, deleted) VALUES ('42177991-308b-488c-b9d5-f50496d08281', '123', 'b3f1b6c8-377c-4fe8-98dc-c4a981498da0', false);


--
-- TOC entry 3023 (class 0 OID 17749)
-- Dependencies: 208
-- Data for Name: loan; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.loan (exitdate, id, returndate, weaponid, licenseeid, reason, initiator, returninitiator) VALUES ('2020-05-29 23:22:16.099+00', '760577ba-eaef-4b16-aa13-ff8ba4fbdd7a', '2020-05-30 15:43:31.796+00', '5a16472b-aae2-4725-902e-c0a137426757', '42177991-308b-488c-b9d5-f50496d08281', NULL, '580a9e08-ea58-473e-b9ff-7f2000522bac', '580a9e08-ea58-473e-b9ff-7f2000522bac');
INSERT INTO public.loan (exitdate, id, returndate, weaponid, licenseeid, reason, initiator, returninitiator) VALUES ('2020-06-07 17:31:23.823+00', '6e20571a-192d-4934-a83d-33632236b353', NULL, 'e3584b30-0fce-425e-b993-b9932017ffe0', '42177991-308b-488c-b9d5-f50496d08281', NULL, '580a9e08-ea58-473e-b9ff-7f2000522bac', NULL);


--
-- TOC entry 3024 (class 0 OID 17756)
-- Dependencies: 209
-- Data for Name: member; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.member (id, name, surname, deleted) VALUES ('b3f1b6c8-377c-4fe8-98dc-c4a981498da0', 'Sami', 'Tahri', false);


--
-- TOC entry 3021 (class 0 OID 17678)
-- Dependencies: 206
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.migrations (id, name, run_on) VALUES (1, '/20200524233759-initDB', '2020-05-25 02:38:46.248');


--
-- TOC entry 3025 (class 0 OID 17764)
-- Dependencies: 210
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public."user" (id, email) VALUES ('db79f02c-ce99-44a6-814f-c95be31084f7', 'admin@admin.com');
INSERT INTO public."user" (id, email) VALUES ('580a9e08-ea58-473e-b9ff-7f2000522bac', 'user@user.com');


--
-- TOC entry 3026 (class 0 OID 17771)
-- Dependencies: 211
-- Data for Name: usercredentials; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.usercredentials (id, userid, password) VALUES ('e4549179-2ade-471f-8d78-a614a7af4eaf', 'db79f02c-ce99-44a6-814f-c95be31084f7', '$2a$10$gjj3XOFhVUPjftYASIKyBOwg680SSYVtG5U8GW9u0E.i1S9l1jPLS');
INSERT INTO public.usercredentials (id, userid, password) VALUES ('dd3c65a9-7e5d-4b63-8bc2-6542d76cf771', '580a9e08-ea58-473e-b9ff-7f2000522bac', '$2a$10$P.u9L5uceWQ1sUVWxhHVfulc9qS/uYjVIGkOsgUmHwrqj.nik4c4O');


--
-- TOC entry 3027 (class 0 OID 17778)
-- Dependencies: 212
-- Data for Name: weapon; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.weapon (id, serialnumber, model, available, description, deleted, handtype, weapontype, calibertype, category, price, clubnumber) VALUES ('0e1c6007-00a8-4255-a969-5d77941998ea', '156ea16', '5za6e16za', true, NULL, true, 'undefined', 'undefined', 'undefined', 'undefined', NULL, NULL);
INSERT INTO public.weapon (id, serialnumber, model, available, description, deleted, handtype, weapontype, calibertype, category, price, clubnumber) VALUES ('5a16472b-aae2-4725-902e-c0a137426757', '123321', '123231', true, '123321', true, 'ambidextrous', 'cannon', 'lever', 'D', 123321, '1569');
INSERT INTO public.weapon (id, serialnumber, model, available, description, deleted, handtype, weapontype, calibertype, category, price, clubnumber) VALUES ('cfa4924f-7e1b-4daa-be6e-503a3f44aa60', '1233211', '', true, 'Allo', true, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.weapon (id, serialnumber, model, available, description, deleted, handtype, weapontype, calibertype, category, price, clubnumber) VALUES ('e3584b30-0fce-425e-b993-b9932017ffe0', 'a546', '8789', false, NULL, false, 'undefined', 'undefined', 'undefined', 'A', NULL, NULL);


--
-- TOC entry 3038 (class 0 OID 0)
-- Dependencies: 205
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.migrations_id_seq', 1, true);


--
-- TOC entry 2870 (class 2606 OID 17788)
-- Name: licensee licensee_licensenumber_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.licensee
    ADD CONSTRAINT licensee_licensenumber_key UNIQUE (licensenumber);


--
-- TOC entry 2872 (class 2606 OID 17790)
-- Name: licensee licensee_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.licensee
    ADD CONSTRAINT licensee_pkey PRIMARY KEY (id);


--
-- TOC entry 2877 (class 2606 OID 17792)
-- Name: loan loan_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT loan_pkey PRIMARY KEY (id);


--
-- TOC entry 2879 (class 2606 OID 17794)
-- Name: member member_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_pkey PRIMARY KEY (id);


--
-- TOC entry 2868 (class 2606 OID 17683)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 17796)
-- Name: user primary_key_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT primary_key_id PRIMARY KEY (id);


--
-- TOC entry 2883 (class 2606 OID 17798)
-- Name: user uniquemail; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT uniquemail UNIQUE (email);


--
-- TOC entry 2886 (class 2606 OID 17800)
-- Name: usercredentials usercredentials_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.usercredentials
    ADD CONSTRAINT usercredentials_pkey PRIMARY KEY (id);


--
-- TOC entry 2888 (class 2606 OID 17802)
-- Name: weapon weapon_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.weapon
    ADD CONSTRAINT weapon_pkey PRIMARY KEY (id);


--
-- TOC entry 2873 (class 1259 OID 17803)
-- Name: fki_fk_licenseeid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_licenseeid ON public.loan USING btree (licenseeid);


--
-- TOC entry 2874 (class 1259 OID 17827)
-- Name: fki_fk_userid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_userid ON public.loan USING btree (initiator);


--
-- TOC entry 2875 (class 1259 OID 17804)
-- Name: fki_fk_weaponid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_fk_weaponid ON public.loan USING btree (weaponid);


--
-- TOC entry 2884 (class 1259 OID 17805)
-- Name: fki_foreignkey_userid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX fki_foreignkey_userid ON public.usercredentials USING btree (userid);


--
-- TOC entry 2889 (class 1259 OID 17806)
-- Name: weapon_serialnumber_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX weapon_serialnumber_idx ON public.weapon USING btree (serialnumber);


--
-- TOC entry 2890 (class 2606 OID 17807)
-- Name: loan fk_licenseeid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_licenseeid FOREIGN KEY (licenseeid) REFERENCES public.licensee(id) NOT VALID;


--
-- TOC entry 2892 (class 2606 OID 17822)
-- Name: loan fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_userid FOREIGN KEY (initiator) REFERENCES public."user"(id) NOT VALID;


--
-- TOC entry 2891 (class 2606 OID 17812)
-- Name: loan fk_weaponid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_weaponid FOREIGN KEY (weaponid) REFERENCES public.weapon(id) NOT VALID;


--
-- TOC entry 2893 (class 2606 OID 17817)
-- Name: usercredentials foreignkey_userid; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.usercredentials
    ADD CONSTRAINT foreignkey_userid FOREIGN KEY (userid) REFERENCES public."user"(id) NOT VALID;


-- Completed on 2020-06-13 18:42:24 UTC

--
-- PostgreSQL database dump complete
--
