--
-- TOC entry 1 (class 3079 OID 16385)
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- TOC entry 3040 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- TOC entry 4 (class 3079 OID 16394)
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- TOC entry 3041 (class 0 OID 0)
-- Dependencies: 4
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- TOC entry 3 (class 3079 OID 16431)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3042 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- TOC entry 214 (class 1259 OID 24731)
-- Name: base_cache_signaling; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.base_cache_signaling
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_cache_signaling OWNER TO admin;

--
-- TOC entry 213 (class 1259 OID 24729)
-- Name: base_registry_signaling; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.base_registry_signaling
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.base_registry_signaling OWNER TO admin;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 205 (class 1259 OID 16442)
-- Name: licensee; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.licensee (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    licensenumber text NOT NULL,
    memberid uuid,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.licensee OWNER TO admin;

--
-- TOC entry 206 (class 1259 OID 16450)
-- Name: loan; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.loan (
    exitdate timestamp with time zone,
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    returndate timestamp with time zone,
    weaponid uuid NOT NULL,
    licenseeid uuid NOT NULL,
    reason text,
    initiator uuid NOT NULL,
    returninitiator uuid
);


ALTER TABLE public.loan OWNER TO admin;

--
-- TOC entry 207 (class 1259 OID 16457)
-- Name: member; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.member (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    name text NOT NULL,
    surname text NOT NULL,
    deleted boolean DEFAULT false NOT NULL
);


ALTER TABLE public.member OWNER TO admin;


CREATE TABLE public."user" (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    email text NOT NULL,
    isdeleted boolean DEFAULT false NOT NULL,
    role text,
    name text,
    surname text
);


ALTER TABLE public."user" OWNER TO admin;

--
-- TOC entry 211 (class 1259 OID 16477)
-- Name: usercredentials; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.usercredentials (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    userid uuid NOT NULL,
    password text NOT NULL
);


ALTER TABLE public.usercredentials OWNER TO admin;

--
-- TOC entry 212 (class 1259 OID 16484)
-- Name: weapon; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.weapon (
    id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    serialnumber text NOT NULL,
    model text,
    available boolean DEFAULT true NOT NULL,
    description text,
    deleted boolean DEFAULT false NOT NULL,
    handtype text,
    weapontype text,
    calibertype text,
    category text,
    price real,
    clubnumber text
);


ALTER TABLE public.weapon OWNER TO admin;



--
-- TOC entry 3025 (class 0 OID 16442)
-- Dependencies: 205
-- Data for Name: licensee; Type: TABLE DATA; Schema: public; Owner: admin
--



--
-- TOC entry 3026 (class 0 OID 16450)
-- Dependencies: 206
-- Data for Name: loan; Type: TABLE DATA; Schema: public; Owner: admin
--



--
-- TOC entry 3027 (class 0 OID 16457)
-- Dependencies: 207
-- Data for Name: member; Type: TABLE DATA; Schema: public; Owner: admin
--




--
-- TOC entry 3030 (class 0 OID 16470)
-- Dependencies: 210
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public."user" (id, email, isdeleted, role, name, surname) VALUES ('580a9e08-ea58-473e-b9ff-7f2000522bac', 'user@user.com', false, 'admin', 'Sami', 'Tahri');
INSERT INTO public."user" (id, email, isdeleted, role, name, surname) VALUES ('db79f02c-ce99-44a6-814f-c95be31084f7', 'admin@admin.com', false, 'superadmin', 'Super', 'Admin');


--
-- TOC entry 3031 (class 0 OID 16477)
-- Dependencies: 211
-- Data for Name: usercredentials; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.usercredentials (id, userid, password) VALUES ('dd3c65a9-7e5d-4b63-8bc2-6542d76cf771', '580a9e08-ea58-473e-b9ff-7f2000522bac', '$2a$10$P.u9L5uceWQ1sUVWxhHVfulc9qS/uYjVIGkOsgUmHwrqj.nik4c4O');
INSERT INTO public.usercredentials (id, userid, password) VALUES ('e4549179-2ade-471f-8d78-a614a7af4eaf', 'db79f02c-ce99-44a6-814f-c95be31084f7', '$2a$10$2Jwk9QAKJCw/bT.USFmUdOvw8FNV7WfL0ruhaV64vqN3xTXp4NiiK');


--
-- TOC entry 3032 (class 0 OID 16484)
-- Dependencies: 212
-- Data for Name: weapon; Type: TABLE DATA; Schema: public; Owner: admin
--



--
-- TOC entry 3044 (class 0 OID 0)
-- Dependencies: 214
-- Name: base_cache_signaling; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.base_cache_signaling', 1, true);


--
-- TOC entry 3045 (class 0 OID 0)
-- Dependencies: 213
-- Name: base_registry_signaling; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.base_registry_signaling', 1, true);



--
-- TOC entry 2873 (class 2606 OID 16495)
-- Name: licensee licensee_licensenumber_key; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.licensee
    ADD CONSTRAINT licensee_licensenumber_key UNIQUE (licensenumber);


--
-- TOC entry 2875 (class 2606 OID 16497)
-- Name: licensee licensee_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.licensee
    ADD CONSTRAINT licensee_pkey PRIMARY KEY (id);


--
-- TOC entry 2880 (class 2606 OID 16499)
-- Name: loan loan_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT loan_pkey PRIMARY KEY (id);


--
-- TOC entry 2882 (class 2606 OID 16501)
-- Name: member member_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT member_pkey PRIMARY KEY (id);



--
-- TOC entry 2886 (class 2606 OID 16505)
-- Name: user primary_key_id; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT primary_key_id PRIMARY KEY (id);


--
-- TOC entry 2888 (class 2606 OID 16507)
-- Name: user uniquemail; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT uniquemail UNIQUE (email);


--
-- TOC entry 2891 (class 2606 OID 16509)
-- Name: usercredentials usercredentials_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.usercredentials
    ADD CONSTRAINT usercredentials_pkey PRIMARY KEY (id);


--
-- TOC entry 2893 (class 2606 OID 16511)
-- Name: weapon weapon_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.weapon
    ADD CONSTRAINT weapon_pkey PRIMARY KEY (id);


--
-- TOC entry 2876 (class 1259 OID 16512)
-- Name: fki_fk_licenseeid; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_fk_licenseeid ON public.loan USING btree (licenseeid);


--
-- TOC entry 2877 (class 1259 OID 16513)
-- Name: fki_fk_userid; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_fk_userid ON public.loan USING btree (initiator);


--
-- TOC entry 2878 (class 1259 OID 16514)
-- Name: fki_fk_weaponid; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_fk_weaponid ON public.loan USING btree (weaponid);


--
-- TOC entry 2889 (class 1259 OID 16515)
-- Name: fki_foreignkey_userid; Type: INDEX; Schema: public; Owner: admin
--

CREATE INDEX fki_foreignkey_userid ON public.usercredentials USING btree (userid);


--
-- TOC entry 2894 (class 1259 OID 16516)
-- Name: weapon_serialnumber_idx; Type: INDEX; Schema: public; Owner: admin
--

CREATE UNIQUE INDEX weapon_serialnumber_idx ON public.weapon USING btree (serialnumber);


--
-- TOC entry 2895 (class 2606 OID 16517)
-- Name: loan fk_licenseeid; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_licenseeid FOREIGN KEY (licenseeid) REFERENCES public.licensee(id) NOT VALID;


--
-- TOC entry 2896 (class 2606 OID 16522)
-- Name: loan fk_userid; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_userid FOREIGN KEY (initiator) REFERENCES public."user"(id) NOT VALID;


--
-- TOC entry 2897 (class 2606 OID 16527)
-- Name: loan fk_weaponid; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.loan
    ADD CONSTRAINT fk_weaponid FOREIGN KEY (weaponid) REFERENCES public.weapon(id) NOT VALID;


--
-- TOC entry 2898 (class 2606 OID 16532)
-- Name: usercredentials foreignkey_userid; Type: FK CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.usercredentials
    ADD CONSTRAINT foreignkey_userid FOREIGN KEY (userid) REFERENCES public."user"(id) NOT VALID;


-- Completed on 2020-07-05 15:58:54 UTC

--
-- PostgreSQL database dump complete
--

